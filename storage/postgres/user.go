package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ibron_uz/ibron_go_user_service/genproto/user_service"
	"gitlab.com/ibron_uz/ibron_go_user_service/models"
	"gitlab.com/ibron_uz/ibron_go_user_service/pkg/helper"
	"gitlab.com/ibron_uz/ibron_go_user_service/storage"
)

type UserRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) storage.UserRepoI {
	return &UserRepo{
		db: db,
	}
}

func (c *UserRepo) Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.UserPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "users" (
				id,
				first_name,
				last_name,
				phone_number,
				date_of_birth,
				gender,
				image_url,
				role_id
			) VALUES ($1, $2, $3, $4, $5, $6, $7, 
				(
					SELECT id FROM "role" WHERE name = 'USER'
				)
			)
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		helper.NewNullString(req.GetFirstName()),
		helper.NewNullString(req.GetLastName()),
		helper.NewNullString(req.GetPhoneNumber()),
		helper.NewNullString(req.GetDateOfBirth()),
		helper.NewNullString(req.GetGender()),
		helper.NewNullString(req.GetImageUrl()),
	)

	if err != nil {
		return nil, err
	}

	return &user_service.UserPrimaryKey{Id: id.String()}, nil
}

func (c *UserRepo) GetByPKey(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error) {

	filter := ""

	if len(req.Id) > 0 {
		filter = " WHERE u.id = '" + req.Id + "' "
	} else if len(req.PhoneNumber) > 0 {
		filter = " WHERE u.phone_number = '" + req.PhoneNumber + "' "
	}

	query := `
		SELECT
			u.id,
			u.first_name,
			u.last_name,
			u.phone_number,
			u.date_of_birth,
			u.gender,
			u.image_url,
			u.role_id,
			
			r.name,
			r.created_at,
			r.updated_at,

			u.active,
			u.created_at,
			u.updated_at
		FROM "users" AS u
		LEFT JOIN "role" AS r ON r.id = u.role_id
	` + filter

	var (
		id            sql.NullString
		firstName     sql.NullString
		lastName      sql.NullString
		phoneNumber   sql.NullString
		dateOfBirth   sql.NullString
		gender        sql.NullString
		imageUrl      sql.NullString
		roleId        sql.NullString
		roleName      sql.NullString
		roleCreatedAt sql.NullString
		roleUpdatedAt sql.NullString
		active        sql.NullInt16
		createdAt     sql.NullString
		updatedAt     sql.NullString
	)

	err = c.db.QueryRow(ctx, query).Scan(
		&id,
		&firstName,
		&lastName,
		&phoneNumber,
		&dateOfBirth,
		&gender,
		&imageUrl,
		&roleId,
		&roleName,
		&roleCreatedAt,
		&roleUpdatedAt,
		&active,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	role := &user_service.Role{
		Id:        roleId.String,
		Name:      roleName.String,
		CreatedAt: roleCreatedAt.String,
		UpdatedAt: roleUpdatedAt.String,
	}

	resp = &user_service.User{
		Id:          id.String,
		FirstName:   firstName.String,
		LastName:    lastName.String,
		PhoneNumber: phoneNumber.String,
		DateOfBirth: dateOfBirth.String,
		Gender:      gender.String,
		ImageUrl:    imageUrl.String,
		RoleId:      roleId.String,
		RoleData:    role,
		Active:      int32(active.Int16),
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *UserRepo) GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error) {

	resp = &user_service.GetListUserResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY u.created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			u.id,
			u.first_name,
			u.last_name,
			u.phone_number,
			u.date_of_birth,
			u.gender,
			u.image_url,
			u.role_id,

			r.name,
			r.created_at,
			r.updated_at,

			u.active,
			u.created_at,
			u.updated_at
		FROM "users" AS u
		LEFT JOIN "role" AS r ON r.id = u.role_id
	`

	if len(req.Search) > 0 {
		filter += " AND u.first_name || u.last_name ILIKE '%' || '" + req.Search + "' || '%' "
	}

	if len(req.RoleId) > 0 {
		filter += " AND role_id = '" + req.RoleId + "' "
	}

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id            sql.NullString
			firstName     sql.NullString
			lastName      sql.NullString
			phoneNumber   sql.NullString
			dateOfBirth   sql.NullString
			gender        sql.NullString
			imageUrl      sql.NullString
			roleId        sql.NullString
			roleName      sql.NullString
			roleCreatedAt sql.NullString
			roleUpdatedAt sql.NullString
			active        sql.NullInt16
			createdAt     sql.NullString
			updatedAt     sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&firstName,
			&lastName,
			&phoneNumber,
			&dateOfBirth,
			&gender,
			&imageUrl,
			&roleId,
			&roleName,
			&roleCreatedAt,
			&roleUpdatedAt,
			&active,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		role := &user_service.Role{
			Id:        roleId.String,
			Name:      roleName.String,
			CreatedAt: roleCreatedAt.String,
			UpdatedAt: roleUpdatedAt.String,
		}

		resp.Users = append(resp.Users, &user_service.User{
			Id:          id.String,
			FirstName:   firstName.String,
			LastName:    lastName.String,
			PhoneNumber: phoneNumber.String,
			DateOfBirth: dateOfBirth.String,
			Gender:      gender.String,
			ImageUrl:    imageUrl.String,
			RoleId:      roleId.String,
			RoleData:    role,
			Active:      int32(active.Int16),
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *UserRepo) Update(ctx context.Context, req *user_service.UpdateUser) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "users"
			SET
				first_name = :first_name,
				last_name = :last_name,
				phone_number = :phone_number,
				date_of_birth = :date_of_birth,
				gender = :gender,
				image_url = :image_url,
				role_id = :role_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":            req.GetId(),
		"first_name":    req.GetFirstName(),
		"last_name":     req.GetLastName(),
		"phone_number":  req.GetPhoneNumber(),
		"date_of_birth": req.GetDateOfBirth(),
		"gender":        req.GetGender(),
		"image_url":     req.GetImageUrl(),
		"role_id":       req.GetRoleId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *UserRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"users"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *UserRepo) Delete(ctx context.Context, req *user_service.UserPrimaryKey) error {

	query := `DELETE FROM "users" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
