package postgres

import (
	"context"
	"database/sql"
	"strings"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ibron_uz/ibron_go_user_service/genproto/user_service"
	"gitlab.com/ibron_uz/ibron_go_user_service/pkg/helper"
	"gitlab.com/ibron_uz/ibron_go_user_service/storage"
)

type RoleRepo struct {
	db *pgxpool.Pool
}

func NewRoleRepo(db *pgxpool.Pool) storage.RoleRepoI {
	return &RoleRepo{
		db: db,
	}
}

func (c *RoleRepo) Create(ctx context.Context, req *user_service.CreateRole) (resp *user_service.RolePrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "role" (
				id,
				name
			) VALUES ($1, $2)
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		strings.ToUpper(req.GetName()),
	)

	if err != nil {
		return nil, err
	}

	return &user_service.RolePrimaryKey{Id: id.String()}, nil
}

func (c *RoleRepo) GetByPKey(ctx context.Context, req *user_service.RolePrimaryKey) (resp *user_service.Role, err error) {

	query := `
		SELECT
			id,
			name,
			created_at,
			updated_at
		FROM "role"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		name      sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Role{
		Id:        id.String,
		Name:      name.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *RoleRepo) GetAll(ctx context.Context, req *user_service.GetListRoleRequest) (resp *user_service.GetListRoleResponse, err error) {

	resp = &user_service.GetListRoleResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			created_at,
			updated_at
		FROM "role"
	`

	if len(req.Search) > 0 {
		filter += " AND name ILIKE '%' || '" + req.Search + "' || '%' "
	}

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Roles = append(resp.Roles, &user_service.Role{
			Id:        id.String,
			Name:      name.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *RoleRepo) Update(ctx context.Context, req *user_service.UpdateRole) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "role"
			SET
				name = :name,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":   req.GetId(),
		"name": req.GetName(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *RoleRepo) Delete(ctx context.Context, req *user_service.RolePrimaryKey) error {

	query := `DELETE FROM "role" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
