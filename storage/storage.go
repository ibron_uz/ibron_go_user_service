package storage

import (
	"context"

	"gitlab.com/ibron_uz/ibron_go_user_service/genproto/user_service"
	"gitlab.com/ibron_uz/ibron_go_user_service/models"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
	Role() RoleRepoI
}

type UserRepoI interface {
	Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.UserPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error)
	GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateUser) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.UserPrimaryKey) error
}

type RoleRepoI interface {
	Create(ctx context.Context, req *user_service.CreateRole) (resp *user_service.RolePrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.RolePrimaryKey) (resp *user_service.Role, err error)
	GetAll(ctx context.Context, req *user_service.GetListRoleRequest) (resp *user_service.GetListRoleResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateRole) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.RolePrimaryKey) error
}
