// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.12.4
// source: payment_history.proto

package payment_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type UserPaymentHistoryPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *UserPaymentHistoryPrimaryKey) Reset() {
	*x = UserPaymentHistoryPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_payment_history_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UserPaymentHistoryPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UserPaymentHistoryPrimaryKey) ProtoMessage() {}

func (x *UserPaymentHistoryPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_payment_history_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UserPaymentHistoryPrimaryKey.ProtoReflect.Descriptor instead.
func (*UserPaymentHistoryPrimaryKey) Descriptor() ([]byte, []int) {
	return file_payment_history_proto_rawDescGZIP(), []int{0}
}

func (x *UserPaymentHistoryPrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type UserPaymentHistory struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id           string  `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	UserId       string  `protobuf:"bytes,2,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	OrderId      string  `protobuf:"bytes,3,opt,name=order_id,json=orderId,proto3" json:"order_id,omitempty"`
	ActivityName string  `protobuf:"bytes,4,opt,name=activity_name,json=activityName,proto3" json:"activity_name,omitempty"`
	CategoryName string  `protobuf:"bytes,5,opt,name=category_name,json=categoryName,proto3" json:"category_name,omitempty"`
	Payme        float32 `protobuf:"fixed32,6,opt,name=payme,proto3" json:"payme,omitempty"`
	Click        float32 `protobuf:"fixed32,7,opt,name=click,proto3" json:"click,omitempty"`
	Cash         float32 `protobuf:"fixed32,8,opt,name=cash,proto3" json:"cash,omitempty"`
	TotalSum     float32 `protobuf:"fixed32,9,opt,name=total_sum,json=totalSum,proto3" json:"total_sum,omitempty"`
	CreatedAt    string  `protobuf:"bytes,10,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt    string  `protobuf:"bytes,11,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *UserPaymentHistory) Reset() {
	*x = UserPaymentHistory{}
	if protoimpl.UnsafeEnabled {
		mi := &file_payment_history_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UserPaymentHistory) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UserPaymentHistory) ProtoMessage() {}

func (x *UserPaymentHistory) ProtoReflect() protoreflect.Message {
	mi := &file_payment_history_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UserPaymentHistory.ProtoReflect.Descriptor instead.
func (*UserPaymentHistory) Descriptor() ([]byte, []int) {
	return file_payment_history_proto_rawDescGZIP(), []int{1}
}

func (x *UserPaymentHistory) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UserPaymentHistory) GetUserId() string {
	if x != nil {
		return x.UserId
	}
	return ""
}

func (x *UserPaymentHistory) GetOrderId() string {
	if x != nil {
		return x.OrderId
	}
	return ""
}

func (x *UserPaymentHistory) GetActivityName() string {
	if x != nil {
		return x.ActivityName
	}
	return ""
}

func (x *UserPaymentHistory) GetCategoryName() string {
	if x != nil {
		return x.CategoryName
	}
	return ""
}

func (x *UserPaymentHistory) GetPayme() float32 {
	if x != nil {
		return x.Payme
	}
	return 0
}

func (x *UserPaymentHistory) GetClick() float32 {
	if x != nil {
		return x.Click
	}
	return 0
}

func (x *UserPaymentHistory) GetCash() float32 {
	if x != nil {
		return x.Cash
	}
	return 0
}

func (x *UserPaymentHistory) GetTotalSum() float32 {
	if x != nil {
		return x.TotalSum
	}
	return 0
}

func (x *UserPaymentHistory) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *UserPaymentHistory) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type CreateUserPaymentHistory struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	UserId       string  `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	OrderId      string  `protobuf:"bytes,2,opt,name=order_id,json=orderId,proto3" json:"order_id,omitempty"`
	ActivityName string  `protobuf:"bytes,3,opt,name=activity_name,json=activityName,proto3" json:"activity_name,omitempty"`
	CategoryName string  `protobuf:"bytes,4,opt,name=category_name,json=categoryName,proto3" json:"category_name,omitempty"`
	Payme        float32 `protobuf:"fixed32,5,opt,name=payme,proto3" json:"payme,omitempty"`
	Click        float32 `protobuf:"fixed32,6,opt,name=click,proto3" json:"click,omitempty"`
	Cash         float32 `protobuf:"fixed32,7,opt,name=cash,proto3" json:"cash,omitempty"`
	TotalSum     float32 `protobuf:"fixed32,8,opt,name=total_sum,json=totalSum,proto3" json:"total_sum,omitempty"`
}

func (x *CreateUserPaymentHistory) Reset() {
	*x = CreateUserPaymentHistory{}
	if protoimpl.UnsafeEnabled {
		mi := &file_payment_history_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateUserPaymentHistory) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateUserPaymentHistory) ProtoMessage() {}

func (x *CreateUserPaymentHistory) ProtoReflect() protoreflect.Message {
	mi := &file_payment_history_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateUserPaymentHistory.ProtoReflect.Descriptor instead.
func (*CreateUserPaymentHistory) Descriptor() ([]byte, []int) {
	return file_payment_history_proto_rawDescGZIP(), []int{2}
}

func (x *CreateUserPaymentHistory) GetUserId() string {
	if x != nil {
		return x.UserId
	}
	return ""
}

func (x *CreateUserPaymentHistory) GetOrderId() string {
	if x != nil {
		return x.OrderId
	}
	return ""
}

func (x *CreateUserPaymentHistory) GetActivityName() string {
	if x != nil {
		return x.ActivityName
	}
	return ""
}

func (x *CreateUserPaymentHistory) GetCategoryName() string {
	if x != nil {
		return x.CategoryName
	}
	return ""
}

func (x *CreateUserPaymentHistory) GetPayme() float32 {
	if x != nil {
		return x.Payme
	}
	return 0
}

func (x *CreateUserPaymentHistory) GetClick() float32 {
	if x != nil {
		return x.Click
	}
	return 0
}

func (x *CreateUserPaymentHistory) GetCash() float32 {
	if x != nil {
		return x.Cash
	}
	return 0
}

func (x *CreateUserPaymentHistory) GetTotalSum() float32 {
	if x != nil {
		return x.TotalSum
	}
	return 0
}

type GetListUserPaymentHistoryRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit  int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	UserId string `protobuf:"bytes,3,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
}

func (x *GetListUserPaymentHistoryRequest) Reset() {
	*x = GetListUserPaymentHistoryRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_payment_history_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListUserPaymentHistoryRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListUserPaymentHistoryRequest) ProtoMessage() {}

func (x *GetListUserPaymentHistoryRequest) ProtoReflect() protoreflect.Message {
	mi := &file_payment_history_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListUserPaymentHistoryRequest.ProtoReflect.Descriptor instead.
func (*GetListUserPaymentHistoryRequest) Descriptor() ([]byte, []int) {
	return file_payment_history_proto_rawDescGZIP(), []int{3}
}

func (x *GetListUserPaymentHistoryRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *GetListUserPaymentHistoryRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetListUserPaymentHistoryRequest) GetUserId() string {
	if x != nil {
		return x.UserId
	}
	return ""
}

type GetListUserPaymentHistoryResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count               int64                 `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	UserPaymentsHistory []*UserPaymentHistory `protobuf:"bytes,2,rep,name=user_payments_history,json=userPaymentsHistory,proto3" json:"user_payments_history,omitempty"`
}

func (x *GetListUserPaymentHistoryResponse) Reset() {
	*x = GetListUserPaymentHistoryResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_payment_history_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListUserPaymentHistoryResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListUserPaymentHistoryResponse) ProtoMessage() {}

func (x *GetListUserPaymentHistoryResponse) ProtoReflect() protoreflect.Message {
	mi := &file_payment_history_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListUserPaymentHistoryResponse.ProtoReflect.Descriptor instead.
func (*GetListUserPaymentHistoryResponse) Descriptor() ([]byte, []int) {
	return file_payment_history_proto_rawDescGZIP(), []int{4}
}

func (x *GetListUserPaymentHistoryResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *GetListUserPaymentHistoryResponse) GetUserPaymentsHistory() []*UserPaymentHistory {
	if x != nil {
		return x.UserPaymentsHistory
	}
	return nil
}

var File_payment_history_proto protoreflect.FileDescriptor

var file_payment_history_proto_rawDesc = []byte{
	0x0a, 0x15, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x5f, 0x68, 0x69, 0x73, 0x74, 0x6f, 0x72,
	0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0f, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x22, 0x2e, 0x0a, 0x1c, 0x55, 0x73, 0x65, 0x72,
	0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x48, 0x69, 0x73, 0x74, 0x6f, 0x72, 0x79, 0x50, 0x72,
	0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0xbd, 0x02, 0x0a, 0x12, 0x55, 0x73, 0x65,
	0x72, 0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x48, 0x69, 0x73, 0x74, 0x6f, 0x72, 0x79, 0x12,
	0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12,
	0x17, 0x0a, 0x07, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x6f, 0x72, 0x64, 0x65,
	0x72, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6f, 0x72, 0x64, 0x65,
	0x72, 0x49, 0x64, 0x12, 0x23, 0x0a, 0x0d, 0x61, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74, 0x79, 0x5f,
	0x6e, 0x61, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x61, 0x63, 0x74, 0x69,
	0x76, 0x69, 0x74, 0x79, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x23, 0x0a, 0x0d, 0x63, 0x61, 0x74, 0x65,
	0x67, 0x6f, 0x72, 0x79, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0c, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x14, 0x0a,
	0x05, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x02, 0x52, 0x05, 0x70, 0x61,
	0x79, 0x6d, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6c, 0x69, 0x63, 0x6b, 0x18, 0x07, 0x20, 0x01,
	0x28, 0x02, 0x52, 0x05, 0x63, 0x6c, 0x69, 0x63, 0x6b, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x61, 0x73,
	0x68, 0x18, 0x08, 0x20, 0x01, 0x28, 0x02, 0x52, 0x04, 0x63, 0x61, 0x73, 0x68, 0x12, 0x1b, 0x0a,
	0x09, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x5f, 0x73, 0x75, 0x6d, 0x18, 0x09, 0x20, 0x01, 0x28, 0x02,
	0x52, 0x08, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x53, 0x75, 0x6d, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72,
	0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09,
	0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64,
	0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0xf5, 0x01, 0x0a, 0x18, 0x43, 0x72, 0x65,
	0x61, 0x74, 0x65, 0x55, 0x73, 0x65, 0x72, 0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x48, 0x69,
	0x73, 0x74, 0x6f, 0x72, 0x79, 0x12, 0x17, 0x0a, 0x07, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x69, 0x64,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x12, 0x19,
	0x0a, 0x08, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x07, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x49, 0x64, 0x12, 0x23, 0x0a, 0x0d, 0x61, 0x63, 0x74,
	0x69, 0x76, 0x69, 0x74, 0x79, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0c, 0x61, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74, 0x79, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x23,
	0x0a, 0x0d, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x4e,
	0x61, 0x6d, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x18, 0x05, 0x20, 0x01,
	0x28, 0x02, 0x52, 0x05, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6c, 0x69,
	0x63, 0x6b, 0x18, 0x06, 0x20, 0x01, 0x28, 0x02, 0x52, 0x05, 0x63, 0x6c, 0x69, 0x63, 0x6b, 0x12,
	0x12, 0x0a, 0x04, 0x63, 0x61, 0x73, 0x68, 0x18, 0x07, 0x20, 0x01, 0x28, 0x02, 0x52, 0x04, 0x63,
	0x61, 0x73, 0x68, 0x12, 0x1b, 0x0a, 0x09, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x5f, 0x73, 0x75, 0x6d,
	0x18, 0x08, 0x20, 0x01, 0x28, 0x02, 0x52, 0x08, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x53, 0x75, 0x6d,
	0x22, 0x69, 0x0a, 0x20, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x55, 0x73, 0x65, 0x72, 0x50,
	0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x48, 0x69, 0x73, 0x74, 0x6f, 0x72, 0x79, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x14, 0x0a, 0x05,
	0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c, 0x69, 0x6d,
	0x69, 0x74, 0x12, 0x17, 0x0a, 0x07, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x22, 0x92, 0x01, 0x0a, 0x21,
	0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x55, 0x73, 0x65, 0x72, 0x50, 0x61, 0x79, 0x6d, 0x65,
	0x6e, 0x74, 0x48, 0x69, 0x73, 0x74, 0x6f, 0x72, 0x79, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x57, 0x0a, 0x15, 0x75, 0x73, 0x65, 0x72, 0x5f,
	0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x5f, 0x68, 0x69, 0x73, 0x74, 0x6f, 0x72, 0x79,
	0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x23, 0x2e, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x55, 0x73, 0x65, 0x72, 0x50, 0x61, 0x79,
	0x6d, 0x65, 0x6e, 0x74, 0x48, 0x69, 0x73, 0x74, 0x6f, 0x72, 0x79, 0x52, 0x13, 0x75, 0x73, 0x65,
	0x72, 0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x48, 0x69, 0x73, 0x74, 0x6f, 0x72, 0x79,
	0x42, 0x1a, 0x5a, 0x18, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x70, 0x61, 0x79,
	0x6d, 0x65, 0x6e, 0x74, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_payment_history_proto_rawDescOnce sync.Once
	file_payment_history_proto_rawDescData = file_payment_history_proto_rawDesc
)

func file_payment_history_proto_rawDescGZIP() []byte {
	file_payment_history_proto_rawDescOnce.Do(func() {
		file_payment_history_proto_rawDescData = protoimpl.X.CompressGZIP(file_payment_history_proto_rawDescData)
	})
	return file_payment_history_proto_rawDescData
}

var file_payment_history_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_payment_history_proto_goTypes = []interface{}{
	(*UserPaymentHistoryPrimaryKey)(nil),      // 0: payment_service.UserPaymentHistoryPrimaryKey
	(*UserPaymentHistory)(nil),                // 1: payment_service.UserPaymentHistory
	(*CreateUserPaymentHistory)(nil),          // 2: payment_service.CreateUserPaymentHistory
	(*GetListUserPaymentHistoryRequest)(nil),  // 3: payment_service.GetListUserPaymentHistoryRequest
	(*GetListUserPaymentHistoryResponse)(nil), // 4: payment_service.GetListUserPaymentHistoryResponse
}
var file_payment_history_proto_depIdxs = []int32{
	1, // 0: payment_service.GetListUserPaymentHistoryResponse.user_payments_history:type_name -> payment_service.UserPaymentHistory
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_payment_history_proto_init() }
func file_payment_history_proto_init() {
	if File_payment_history_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_payment_history_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UserPaymentHistoryPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_payment_history_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UserPaymentHistory); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_payment_history_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateUserPaymentHistory); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_payment_history_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListUserPaymentHistoryRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_payment_history_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListUserPaymentHistoryResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_payment_history_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_payment_history_proto_goTypes,
		DependencyIndexes: file_payment_history_proto_depIdxs,
		MessageInfos:      file_payment_history_proto_msgTypes,
	}.Build()
	File_payment_history_proto = out.File
	file_payment_history_proto_rawDesc = nil
	file_payment_history_proto_goTypes = nil
	file_payment_history_proto_depIdxs = nil
}
