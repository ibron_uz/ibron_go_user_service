// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.12.4
// source: user_order_history_service.proto

package order_service

import (
	context "context"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// UserOrderHistoryServiceClient is the client API for UserOrderHistoryService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type UserOrderHistoryServiceClient interface {
	GetList(ctx context.Context, in *GetListUserOrderHistoryRequest, opts ...grpc.CallOption) (*GetListUserOrderHistoryResponse, error)
	Delete(ctx context.Context, in *UserOrderHistoryPrimaryKey, opts ...grpc.CallOption) (*empty.Empty, error)
}

type userOrderHistoryServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewUserOrderHistoryServiceClient(cc grpc.ClientConnInterface) UserOrderHistoryServiceClient {
	return &userOrderHistoryServiceClient{cc}
}

func (c *userOrderHistoryServiceClient) GetList(ctx context.Context, in *GetListUserOrderHistoryRequest, opts ...grpc.CallOption) (*GetListUserOrderHistoryResponse, error) {
	out := new(GetListUserOrderHistoryResponse)
	err := c.cc.Invoke(ctx, "/order_service.UserOrderHistoryService/GetList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userOrderHistoryServiceClient) Delete(ctx context.Context, in *UserOrderHistoryPrimaryKey, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/order_service.UserOrderHistoryService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// UserOrderHistoryServiceServer is the server API for UserOrderHistoryService service.
// All implementations must embed UnimplementedUserOrderHistoryServiceServer
// for forward compatibility
type UserOrderHistoryServiceServer interface {
	GetList(context.Context, *GetListUserOrderHistoryRequest) (*GetListUserOrderHistoryResponse, error)
	Delete(context.Context, *UserOrderHistoryPrimaryKey) (*empty.Empty, error)
	mustEmbedUnimplementedUserOrderHistoryServiceServer()
}

// UnimplementedUserOrderHistoryServiceServer must be embedded to have forward compatible implementations.
type UnimplementedUserOrderHistoryServiceServer struct {
}

func (UnimplementedUserOrderHistoryServiceServer) GetList(context.Context, *GetListUserOrderHistoryRequest) (*GetListUserOrderHistoryResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedUserOrderHistoryServiceServer) Delete(context.Context, *UserOrderHistoryPrimaryKey) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedUserOrderHistoryServiceServer) mustEmbedUnimplementedUserOrderHistoryServiceServer() {
}

// UnsafeUserOrderHistoryServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to UserOrderHistoryServiceServer will
// result in compilation errors.
type UnsafeUserOrderHistoryServiceServer interface {
	mustEmbedUnimplementedUserOrderHistoryServiceServer()
}

func RegisterUserOrderHistoryServiceServer(s grpc.ServiceRegistrar, srv UserOrderHistoryServiceServer) {
	s.RegisterService(&UserOrderHistoryService_ServiceDesc, srv)
}

func _UserOrderHistoryService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetListUserOrderHistoryRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserOrderHistoryServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/order_service.UserOrderHistoryService/GetList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserOrderHistoryServiceServer).GetList(ctx, req.(*GetListUserOrderHistoryRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserOrderHistoryService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UserOrderHistoryPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserOrderHistoryServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/order_service.UserOrderHistoryService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserOrderHistoryServiceServer).Delete(ctx, req.(*UserOrderHistoryPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

// UserOrderHistoryService_ServiceDesc is the grpc.ServiceDesc for UserOrderHistoryService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var UserOrderHistoryService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "order_service.UserOrderHistoryService",
	HandlerType: (*UserOrderHistoryServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetList",
			Handler:    _UserOrderHistoryService_GetList_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _UserOrderHistoryService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "user_order_history_service.proto",
}
