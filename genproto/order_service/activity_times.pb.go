// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.12.4
// source: activity_times.proto

package order_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type ActivityTimePrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *ActivityTimePrimaryKey) Reset() {
	*x = ActivityTimePrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_activity_times_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ActivityTimePrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ActivityTimePrimaryKey) ProtoMessage() {}

func (x *ActivityTimePrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_activity_times_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ActivityTimePrimaryKey.ProtoReflect.Descriptor instead.
func (*ActivityTimePrimaryKey) Descriptor() ([]byte, []int) {
	return file_activity_times_proto_rawDescGZIP(), []int{0}
}

func (x *ActivityTimePrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type ActivityTime struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	StartAt    string `protobuf:"bytes,2,opt,name=start_at,json=startAt,proto3" json:"start_at,omitempty"`
	EndAt      string `protobuf:"bytes,3,opt,name=end_at,json=endAt,proto3" json:"end_at,omitempty"`
	ActivityId string `protobuf:"bytes,4,opt,name=activity_id,json=activityId,proto3" json:"activity_id,omitempty"`
	CreatedAt  string `protobuf:"bytes,5,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt  string `protobuf:"bytes,6,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *ActivityTime) Reset() {
	*x = ActivityTime{}
	if protoimpl.UnsafeEnabled {
		mi := &file_activity_times_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ActivityTime) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ActivityTime) ProtoMessage() {}

func (x *ActivityTime) ProtoReflect() protoreflect.Message {
	mi := &file_activity_times_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ActivityTime.ProtoReflect.Descriptor instead.
func (*ActivityTime) Descriptor() ([]byte, []int) {
	return file_activity_times_proto_rawDescGZIP(), []int{1}
}

func (x *ActivityTime) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *ActivityTime) GetStartAt() string {
	if x != nil {
		return x.StartAt
	}
	return ""
}

func (x *ActivityTime) GetEndAt() string {
	if x != nil {
		return x.EndAt
	}
	return ""
}

func (x *ActivityTime) GetActivityId() string {
	if x != nil {
		return x.ActivityId
	}
	return ""
}

func (x *ActivityTime) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *ActivityTime) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type CreateActivityTime struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	StartAt    string `protobuf:"bytes,1,opt,name=start_at,json=startAt,proto3" json:"start_at,omitempty"`
	EndAt      string `protobuf:"bytes,2,opt,name=end_at,json=endAt,proto3" json:"end_at,omitempty"`
	ActivityId string `protobuf:"bytes,3,opt,name=activity_id,json=activityId,proto3" json:"activity_id,omitempty"`
}

func (x *CreateActivityTime) Reset() {
	*x = CreateActivityTime{}
	if protoimpl.UnsafeEnabled {
		mi := &file_activity_times_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateActivityTime) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateActivityTime) ProtoMessage() {}

func (x *CreateActivityTime) ProtoReflect() protoreflect.Message {
	mi := &file_activity_times_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateActivityTime.ProtoReflect.Descriptor instead.
func (*CreateActivityTime) Descriptor() ([]byte, []int) {
	return file_activity_times_proto_rawDescGZIP(), []int{2}
}

func (x *CreateActivityTime) GetStartAt() string {
	if x != nil {
		return x.StartAt
	}
	return ""
}

func (x *CreateActivityTime) GetEndAt() string {
	if x != nil {
		return x.EndAt
	}
	return ""
}

func (x *CreateActivityTime) GetActivityId() string {
	if x != nil {
		return x.ActivityId
	}
	return ""
}

type UpdateActivityTime struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	StartAt    string `protobuf:"bytes,2,opt,name=start_at,json=startAt,proto3" json:"start_at,omitempty"`
	EndAt      string `protobuf:"bytes,3,opt,name=end_at,json=endAt,proto3" json:"end_at,omitempty"`
	ActivityId string `protobuf:"bytes,4,opt,name=activity_id,json=activityId,proto3" json:"activity_id,omitempty"`
}

func (x *UpdateActivityTime) Reset() {
	*x = UpdateActivityTime{}
	if protoimpl.UnsafeEnabled {
		mi := &file_activity_times_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateActivityTime) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateActivityTime) ProtoMessage() {}

func (x *UpdateActivityTime) ProtoReflect() protoreflect.Message {
	mi := &file_activity_times_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateActivityTime.ProtoReflect.Descriptor instead.
func (*UpdateActivityTime) Descriptor() ([]byte, []int) {
	return file_activity_times_proto_rawDescGZIP(), []int{3}
}

func (x *UpdateActivityTime) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdateActivityTime) GetStartAt() string {
	if x != nil {
		return x.StartAt
	}
	return ""
}

func (x *UpdateActivityTime) GetEndAt() string {
	if x != nil {
		return x.EndAt
	}
	return ""
}

func (x *UpdateActivityTime) GetActivityId() string {
	if x != nil {
		return x.ActivityId
	}
	return ""
}

type GetListActivityTimeRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset     int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit      int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	ActivityId string `protobuf:"bytes,3,opt,name=activity_id,json=activityId,proto3" json:"activity_id,omitempty"`
}

func (x *GetListActivityTimeRequest) Reset() {
	*x = GetListActivityTimeRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_activity_times_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListActivityTimeRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListActivityTimeRequest) ProtoMessage() {}

func (x *GetListActivityTimeRequest) ProtoReflect() protoreflect.Message {
	mi := &file_activity_times_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListActivityTimeRequest.ProtoReflect.Descriptor instead.
func (*GetListActivityTimeRequest) Descriptor() ([]byte, []int) {
	return file_activity_times_proto_rawDescGZIP(), []int{4}
}

func (x *GetListActivityTimeRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *GetListActivityTimeRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetListActivityTimeRequest) GetActivityId() string {
	if x != nil {
		return x.ActivityId
	}
	return ""
}

type GetListActivityTimeResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count         int64           `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	ActivityTimes []*ActivityTime `protobuf:"bytes,2,rep,name=activity_times,json=activityTimes,proto3" json:"activity_times,omitempty"`
}

func (x *GetListActivityTimeResponse) Reset() {
	*x = GetListActivityTimeResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_activity_times_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListActivityTimeResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListActivityTimeResponse) ProtoMessage() {}

func (x *GetListActivityTimeResponse) ProtoReflect() protoreflect.Message {
	mi := &file_activity_times_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListActivityTimeResponse.ProtoReflect.Descriptor instead.
func (*GetListActivityTimeResponse) Descriptor() ([]byte, []int) {
	return file_activity_times_proto_rawDescGZIP(), []int{5}
}

func (x *GetListActivityTimeResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *GetListActivityTimeResponse) GetActivityTimes() []*ActivityTime {
	if x != nil {
		return x.ActivityTimes
	}
	return nil
}

var File_activity_times_proto protoreflect.FileDescriptor

var file_activity_times_proto_rawDesc = []byte{
	0x0a, 0x14, 0x61, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74, 0x79, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x73,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0d, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x22, 0x28, 0x0a, 0x16, 0x41, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74,
	0x79, 0x54, 0x69, 0x6d, 0x65, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x12,
	0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22,
	0xaf, 0x01, 0x0a, 0x0c, 0x41, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74, 0x79, 0x54, 0x69, 0x6d, 0x65,
	0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64,
	0x12, 0x19, 0x0a, 0x08, 0x73, 0x74, 0x61, 0x72, 0x74, 0x5f, 0x61, 0x74, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x07, 0x73, 0x74, 0x61, 0x72, 0x74, 0x41, 0x74, 0x12, 0x15, 0x0a, 0x06, 0x65,
	0x6e, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65, 0x6e, 0x64,
	0x41, 0x74, 0x12, 0x1f, 0x0a, 0x0b, 0x61, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74, 0x79, 0x5f, 0x69,
	0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x61, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74,
	0x79, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61,
	0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64,
	0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74,
	0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41,
	0x74, 0x22, 0x67, 0x0a, 0x12, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x41, 0x63, 0x74, 0x69, 0x76,
	0x69, 0x74, 0x79, 0x54, 0x69, 0x6d, 0x65, 0x12, 0x19, 0x0a, 0x08, 0x73, 0x74, 0x61, 0x72, 0x74,
	0x5f, 0x61, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x73, 0x74, 0x61, 0x72, 0x74,
	0x41, 0x74, 0x12, 0x15, 0x0a, 0x06, 0x65, 0x6e, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x05, 0x65, 0x6e, 0x64, 0x41, 0x74, 0x12, 0x1f, 0x0a, 0x0b, 0x61, 0x63, 0x74,
	0x69, 0x76, 0x69, 0x74, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a,
	0x61, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74, 0x79, 0x49, 0x64, 0x22, 0x77, 0x0a, 0x12, 0x55, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x41, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74, 0x79, 0x54, 0x69, 0x6d, 0x65,
	0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64,
	0x12, 0x19, 0x0a, 0x08, 0x73, 0x74, 0x61, 0x72, 0x74, 0x5f, 0x61, 0x74, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x07, 0x73, 0x74, 0x61, 0x72, 0x74, 0x41, 0x74, 0x12, 0x15, 0x0a, 0x06, 0x65,
	0x6e, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65, 0x6e, 0x64,
	0x41, 0x74, 0x12, 0x1f, 0x0a, 0x0b, 0x61, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74, 0x79, 0x5f, 0x69,
	0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x61, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74,
	0x79, 0x49, 0x64, 0x22, 0x6b, 0x0a, 0x1a, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x41, 0x63,
	0x74, 0x69, 0x76, 0x69, 0x74, 0x79, 0x54, 0x69, 0x6d, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x12, 0x16, 0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x03, 0x52, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d,
	0x69, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12,
	0x1f, 0x0a, 0x0b, 0x61, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x61, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74, 0x79, 0x49, 0x64,
	0x22, 0x77, 0x0a, 0x1b, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x41, 0x63, 0x74, 0x69, 0x76,
	0x69, 0x74, 0x79, 0x54, 0x69, 0x6d, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05,
	0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x42, 0x0a, 0x0e, 0x61, 0x63, 0x74, 0x69, 0x76, 0x69, 0x74,
	0x79, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x1b, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x41, 0x63,
	0x74, 0x69, 0x76, 0x69, 0x74, 0x79, 0x54, 0x69, 0x6d, 0x65, 0x52, 0x0d, 0x61, 0x63, 0x74, 0x69,
	0x76, 0x69, 0x74, 0x79, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x42, 0x18, 0x5a, 0x16, 0x67, 0x65, 0x6e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_activity_times_proto_rawDescOnce sync.Once
	file_activity_times_proto_rawDescData = file_activity_times_proto_rawDesc
)

func file_activity_times_proto_rawDescGZIP() []byte {
	file_activity_times_proto_rawDescOnce.Do(func() {
		file_activity_times_proto_rawDescData = protoimpl.X.CompressGZIP(file_activity_times_proto_rawDescData)
	})
	return file_activity_times_proto_rawDescData
}

var file_activity_times_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_activity_times_proto_goTypes = []interface{}{
	(*ActivityTimePrimaryKey)(nil),      // 0: order_service.ActivityTimePrimaryKey
	(*ActivityTime)(nil),                // 1: order_service.ActivityTime
	(*CreateActivityTime)(nil),          // 2: order_service.CreateActivityTime
	(*UpdateActivityTime)(nil),          // 3: order_service.UpdateActivityTime
	(*GetListActivityTimeRequest)(nil),  // 4: order_service.GetListActivityTimeRequest
	(*GetListActivityTimeResponse)(nil), // 5: order_service.GetListActivityTimeResponse
}
var file_activity_times_proto_depIdxs = []int32{
	1, // 0: order_service.GetListActivityTimeResponse.activity_times:type_name -> order_service.ActivityTime
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_activity_times_proto_init() }
func file_activity_times_proto_init() {
	if File_activity_times_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_activity_times_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ActivityTimePrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_activity_times_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ActivityTime); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_activity_times_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateActivityTime); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_activity_times_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateActivityTime); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_activity_times_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListActivityTimeRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_activity_times_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListActivityTimeResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_activity_times_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_activity_times_proto_goTypes,
		DependencyIndexes: file_activity_times_proto_depIdxs,
		MessageInfos:      file_activity_times_proto_msgTypes,
	}.Build()
	File_activity_times_proto = out.File
	file_activity_times_proto_rawDesc = nil
	file_activity_times_proto_goTypes = nil
	file_activity_times_proto_depIdxs = nil
}
