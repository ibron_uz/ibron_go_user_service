CREATE TABLE if not exists "role" (
  "id" UUID PRIMARY KEY NOT NULL,
  "name" VARCHAR(60) UNIQUE NOT NULL,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp DEFAULT (CURRENT_TIMESTAMP)
);

CREATE TABLE if not exists "users" (
  "id" UUID PRIMARY KEY NOT NULL,
  "first_name" VARCHAR(60),
  "last_name" VARCHAR(60),
  "phone_number" VARCHAR(20) UNIQUE NOT NULL,
  "date_of_birth" DATE,
  "gender" VARCHAR DEFAULT 'male' CHECK(gender in ('male', 'female')),
  "image_url" VARCHAR(70),
  "role_id" UUID REFERENCES "role" ("id"),
  "active" SMALLINT DEFAULT 1,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp DEFAULT (CURRENT_TIMESTAMP)
);
