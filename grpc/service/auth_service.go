package service

import (
	"context"
	"errors"
	"time"

	"gitlab.com/ibron_uz/ibron_go_user_service/config"
	"gitlab.com/ibron_uz/ibron_go_user_service/genproto/user_service"
	"gitlab.com/ibron_uz/ibron_go_user_service/grpc/client"
	"gitlab.com/ibron_uz/ibron_go_user_service/pkg/helper"
	"gitlab.com/ibron_uz/ibron_go_user_service/pkg/logger"
	"gitlab.com/ibron_uz/ibron_go_user_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type AuthService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedAuthServiceServer
}

func NewAuthService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *AuthService {
	return &AuthService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

var otpCodes = map[string]string{}

func (i *AuthService) GenerateOTP(ctx context.Context, req *user_service.OTPRegisterRequest) (*user_service.OTPRegisterResponse, error) {
	i.log.Info("---Auth Generate OTP------>", logger.Any("req", req))

	// Generate a random secret key
	code, err := helper.GenerateOTP(4)
	if err != nil {
		i.log.Error("!!!AUTH->GENEREATECODE->TOTP--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// store otp code temprorary
	otpCodes[req.GetPhoneNumber()] = code

	// send sms to phone number logics

	// -------------------------------

	i.log.Info("---GENERATED CODE FOR 30 sec------>", logger.Any("code", code))

	// remove code after 60 sec
	time.AfterFunc(60*time.Second, func() {
		delete(otpCodes, req.PhoneNumber)
	})

	return &user_service.OTPRegisterResponse{
		PhoneNumber: req.GetPhoneNumber(),
	}, nil
}

func (i *AuthService) VerifyOTP(ctx context.Context, req *user_service.VerifyOTPRequest) (resp *user_service.AccessToken, err error) {
	i.log.Info("---Auth VerifyOTP------>", logger.Any("req", req))

	v, ok := otpCodes[req.PhoneNumber]
	if !ok {
		i.log.Error("!!!AUTH->Verify->OTP--->", logger.Error(errors.New("otp time expired")))
		return nil, status.Error(codes.InvalidArgument, "otp time expired")
	}

	if v != req.Code {
		i.log.Error("!!!AUTH->Verify->OTP--->", logger.Error(errors.New("incorrect code")))
		return nil, status.Error(codes.InvalidArgument, "incorrect code")
	}

	// if verified delete code
	delete(otpCodes, req.PhoneNumber)

	// generate token logics sign-up - login

	user := &user_service.User{}

	user, err = i.strg.User().GetByPKey(ctx, &user_service.UserPrimaryKey{
		PhoneNumber: req.PhoneNumber,
	})
	if err != nil {
		if err.Error() == "no rows in result set" {
			userId, err := i.strg.User().Create(ctx, &user_service.CreateUser{
				PhoneNumber: req.PhoneNumber,
			})
			if err != nil {
				i.log.Error("!!!USER->CREATE->AUTH--->", logger.Error(err))
				return nil, status.Error(codes.InvalidArgument, err.Error())
			}

			user, err = i.strg.User().GetByPKey(ctx, &user_service.UserPrimaryKey{
				Id: userId.Id,
			})
			if err != nil {
				i.log.Error("!!!USER->GetByPKey->AUTH--->", logger.Error(err))
				return nil, status.Error(codes.InvalidArgument, err.Error())
			}

		} else {
			i.log.Error("!!!AUTH->Verify->AUTH--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	}

	// generate token for user
	data := map[string]interface{}{
		"Id":        user.Id,
		"user_type": user.RoleData.Name,
	}

	token, err := helper.GenerateJWT(data, config.TimeExpiredAt, i.cfg.SecretKey)
	if err != nil {
		i.log.Error("!!!AUTH->GenerateJWT->TOKEN--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &user_service.AccessToken{
		Token: token,
	}, nil
}
