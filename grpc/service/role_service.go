package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/ibron_uz/ibron_go_user_service/config"
	"gitlab.com/ibron_uz/ibron_go_user_service/genproto/user_service"
	"gitlab.com/ibron_uz/ibron_go_user_service/grpc/client"
	"gitlab.com/ibron_uz/ibron_go_user_service/pkg/logger"
	"gitlab.com/ibron_uz/ibron_go_user_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type RoleService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedRoleServiceServer
}

func NewRoleService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *RoleService {
	return &RoleService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *RoleService) Create(ctx context.Context, req *user_service.CreateRole) (resp *user_service.Role, err error) {

	i.log.Info("---CreateRole------>", logger.Any("req", req))

	pKey, err := i.strg.Role().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateRole->Role->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Role().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyRole->Role->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *RoleService) GetByID(ctx context.Context, req *user_service.RolePrimaryKey) (resp *user_service.Role, err error) {

	i.log.Info("---GetRoleByID------>", logger.Any("req", req))

	resp, err = i.strg.Role().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetRoleByID->Role->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *RoleService) GetList(ctx context.Context, req *user_service.GetListRoleRequest) (resp *user_service.GetListRoleResponse, err error) {

	i.log.Info("---GetRoles------>", logger.Any("req", req))

	resp, err = i.strg.Role().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetRoles->Role->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *RoleService) Update(ctx context.Context, req *user_service.UpdateRole) (resp *user_service.Role, err error) {

	i.log.Info("---UpdateRole------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Role().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateRole--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Role().GetByPKey(ctx, &user_service.RolePrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetRole->Role->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *RoleService) Delete(ctx context.Context, req *user_service.RolePrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteRole------>", logger.Any("req", req))

	err = i.strg.Role().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteRole->Role->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
