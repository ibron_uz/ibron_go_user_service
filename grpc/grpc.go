package grpc

import (
	"gitlab.com/ibron_uz/ibron_go_user_service/config"
	"gitlab.com/ibron_uz/ibron_go_user_service/genproto/user_service"
	"gitlab.com/ibron_uz/ibron_go_user_service/grpc/client"
	"gitlab.com/ibron_uz/ibron_go_user_service/grpc/service"
	"gitlab.com/ibron_uz/ibron_go_user_service/pkg/logger"
	"gitlab.com/ibron_uz/ibron_go_user_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	user_service.RegisterUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))
	user_service.RegisterRoleServiceServer(grpcServer, service.NewRoleService(cfg, log, strg, srvc))
	user_service.RegisterAuthServiceServer(grpcServer, service.NewAuthService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
